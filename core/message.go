package core

import (
	"strings"
)

// Message for chat
type Message struct {
	Author string
	Content string
}

// IsBuy ...
func (msg *Message) IsBuy() bool {
	res := msg.Equals("buy")
	return res 
}

// IsList ...
func (msg *Message) IsList() bool {
	res := msg.Equals("list")
	return res || msg.Author == "list"
}

// ToString ...
func (msg *Message) ToString() string {
	var cnt string
	if msg.IsBuy() {
		cnt = "Good Buy my dear friends"
	} else {
		cnt = msg.Content
	}
	var pref string
	if msg.IsList(){
		pref = ""
	} else {
		pref = "*" + msg.Author + ": " 
	}
	res := pref + cnt
	return res 
}

// Equals ...
func (msg *Message) Equals(str string) bool {
	preparedMsg := strings.Trim(msg.Content, " ")
	res := strings.EqualFold(preparedMsg, str + "\r\n")
	return res
}

