package main

import (
    "fmt"
    "net"
    core "console-chat/core"
	"encoding/gob"
    "container/list"
    "strings"
	"os"
	"bufio"
)

type client struct {
    name string
    enabled bool
    incoming chan core.Message
    outgoing chan core.Message
}     

func clientRead(client *client, stop *bool, decoder *gob.Decoder) {
    go func() {
       for !(*stop) {
            var msg core.Message
            err := decoder.Decode(&msg)
            if err != nil {
                if strings.HasPrefix(err.Error(), "extra data in buffer") {
                    continue
                }
                
                // close
                client.enabled = false
                client.incoming <- msg
                return
            }
            client.name = msg.Author
            client.incoming <- msg
        } 
    }()
}

func clientWrite(client *client, stop *bool, encoder *gob.Encoder) {
    go func() {
        for msg := range client.outgoing {
            if (*stop) {
                return
            }
            encoder.Encode(msg)   
        }
    }()
}

func createClient(conn net.Conn) (*client, *bool) {
    decoder := gob.NewDecoder(conn)
    encoder := gob.NewEncoder(conn)
    stop := false
    
    client := &client{
        enabled: true,
		incoming: make(chan core.Message),
		outgoing: make(chan core.Message),
	}

    clientRead(client, &stop, decoder)
	clientWrite(client, &stop, encoder)
    
    return client, &stop
}

func broadcast(clients *list.List, msg *core.Message) {
    for el := clients.Front(); el != nil; el=el.Next() {
        client := el.Value.(*client) 
        client.outgoing <- (*msg)
    }
}

func handleList(clients *list.List, encoder *gob.Encoder) {
    txt := ""
    for el := clients.Front(); el != nil; el=el.Next() {
        cl1, ok := el.Value.(*client)
        if !ok {
            continue
        }
        txt = txt + cl1.name + "\n"
    } 
    msg1 := core.Message {Author: "list", Content: txt} 
    encoder.Encode(msg1)
}

func listenClientIncoming(
    cl *client,
    clients *list.List,
    incoming chan<- core.Message,
    history chan<- core.Message,
    encoder *gob.Encoder) {
    
    registered := false
    var msg core.Message
    for {  
        msg = <-cl.incoming
        if !cl.enabled {
            break
        }
        if !registered {
            fmt.Println(cl.name, " is online")
            registered = true
        }
        if msg.IsList() { 
            handleList(clients, encoder)
            continue
        }
        incoming <- msg
        history <- msg
        if msg.IsBuy() {
            break
        }
    }
}

func join(
    ln *net.Listener,
    incoming chan<- core.Message,
    clients *list.List,
    history chan<- core.Message,
    lastMessages *[]core.Message) {
    
    conn, err := (*ln).Accept()
    if err != nil {
        fmt.Println(err)
        return
    }
    defer conn.Close()
    
    // send last 10 messages 
    encoder := gob.NewEncoder(conn)
    for _, msg := range (*lastMessages) {
        encoder.Encode(msg)   
    }
    
    cl, stopClient := createClient(conn)
    el := clients.PushBack(cl)

    go join(ln, incoming, clients, history, lastMessages)
    
    listenClientIncoming(cl, clients, incoming, history, encoder)
    
    *stopClient = true
    clients.Remove(el)
    fmt.Println(cl.name, " is offline")
}
 
func chatRoom(
    socket string,
    clients *list.List,
    incoming chan core.Message,
    history chan<- core.Message,
    lastMessages *[]core.Message) {
        
    ln, err := net.Listen("tcp", socket)
    if err != nil {
        fmt.Println(err)
        return
    }
    
    go join(&ln, incoming, clients, history, lastMessages)
    
    for {
        msg := <-incoming
        broadcast(clients, &msg)
    }
}

func history(incoming <-chan core.Message) *[]core.Message {
    var lastMessages []core.Message
       
    go func() {
        for {
            msg := <-incoming
            lastMessages = append(lastMessages, msg)
            if len(lastMessages) > 10 {
                lastMessages = lastMessages[1:]   
            }
        }
    }() 
    
    return &lastMessages
}

func listenModeratorInput(incoming chan<- core.Message) {
    for {
        in := bufio.NewReader(os.Stdin)
        input, _ := in.ReadString('\n')
        
        msg := core.Message {
            Author: "moderator",
            Content: input,
        }
        
        incoming <- msg
    }
}

func main() {
    
    args := os.Args[1:]
    if len(args) != 1 {
        fmt.Println("Incorrect number of arguments. Must be one: socket.")
    }
    
    socket := args[0]
    incoming := make(chan core.Message)
    histIncoming := make(chan core.Message)
    
    lastMessages := history(histIncoming)
    clients := list.New()
    go chatRoom(socket, clients, incoming, histIncoming, lastMessages)
    
	listenModeratorInput(incoming)
}