# README #

### Follow the next steps: ###

* Clone repository to your Go project source directory (for me `C:\Projects\Go\src`)
* Start server `go run ./server/main.go localhost:9999` (I developed it on Windows 10)
* Start client `go run ./client/main.go localhost:9999 dima`
* Enjoy