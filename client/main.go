package main

import (
	"net"
    core "console-chat/core"
	"encoding/gob"
	"fmt"
	"os"
	"bufio"
	"strings"
)

type messageAction func (msg *core.Message) error

func isServerResponseError(err error) bool {
    return !strings.HasPrefix(err.Error(), "extra data in buffer")
}

func serverResponseErrorPanic(err error) {
    panic("Error. Server doesn't response: " + err.Error())
}

func listenServer(conn net.Conn, print messageAction) {
    dec := gob.NewDecoder(conn)
    for {
        var msg core.Message
        err := dec.Decode(&msg)
        if err != nil {
            if !isServerResponseError(err) {
                continue
            }
            serverResponseErrorPanic(err)
        }
        print(&msg)
    } 
}

func printOutputMessage(msg *core.Message) error {
    fmt.Println("\n", msg.ToString())
    return nil
}

func listenUserInput(author string, send messageAction) {
    for {
        in := bufio.NewReader(os.Stdin)
        line, _ := in.ReadString('\n')
        
        msg := core.Message{
            Author: author,
            Content: line,
        }
        
        err := send(&msg)
        if err != nil {
            if !isServerResponseError(err) {
                continue
            }
            serverResponseErrorPanic(err)
        }
        
        if msg.IsBuy() {
            fmt.Println("\nquit")
            return
        }
    }
}

func sendInputMessage(conn net.Conn, msg *core.Message) error {
    err := gob.NewEncoder(conn).Encode(&msg)
    return err
}

func main() {
    
    args := os.Args[1:]
    if len(args) != 2 {
        fmt.Println("Incorrect number of arguments. Must be two: socket, author.")
    }
    
    socket := args[0]
    author := args[1]
    
    if author == "list" {
        fmt.Println("list is incorrect name (reserved).")
        return
    }
    
    conn, err := net.Dial("tcp", socket)
    if err != nil {
        fmt.Println(err)
        return
    }
    defer conn.Close()
    
    fmt.Println("Connected to ", socket + "\n")
    
    go listenServer(conn, printOutputMessage)    
    
    send := func(msg *core.Message) error {
        return sendInputMessage(conn, msg)
    }
    
    listenUserInput(author, send)
    
}